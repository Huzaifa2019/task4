package com.example.task4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class NextActivity extends AppCompatActivity {
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        submit = (Button) findViewById(R.id.button);


        TextView tv2 = (TextView)findViewById(R.id.textView2);
        TextView tv3 = (TextView)findViewById(R.id.textView3);
        TextView tv4 = (TextView)findViewById(R.id.textView4);

        Intent intent = getIntent();

        String str = intent.getStringExtra("first");
        tv2.setText("First Name: " + str);
        String str2 = intent.getStringExtra("last");
        tv3.setText("Last Name: " + str2);
        String str3 = intent.getStringExtra("email");
        tv4.setText("Email: " + str3);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}